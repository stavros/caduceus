from caduceus.cli import load_config


def test_config():
    assert type(load_config(config_path="caduceus.toml.example")) == dict
