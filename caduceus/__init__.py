# -*- coding: utf-8 -*-

"A server that will alert you if your tasks have not run."

__author__ = "Stavros Korokithakis"
__email__ = "hi@stavros.io"
__version__ = "0.0.11"
